Swift SQLite
==============

Easy to use access to sqlite databases in Swift.

Usage
======

Simple sql flow:
```swift
let conn = try Connection(target: .memory)

let create = try conn.statement("CREATE TABLE IF NOT EXISTS users (name, email)")
try create.execute()

let insert = try conn.statement("INSERT INTO users VALUES (?1, ?2)")
try insert.string("John Smith", to: 1)
try insert.string("jsmith@example.com", to: 2)
try insert.execute(andClose: false)

let all = try conn.statement("SELECT rowid, name, email FROM users")

var row = try all.next()
while let currentRow = row {
    var summary = ""
    for i in 0..<(try currentRow.count()) {
        summary += "\(try currentRow.name(from: i)) (\(try currentRow.string(from: i))) "
    }
    print(summary)
    row = try currentRow.next()
}

try all.close()

try conn.close()
```

Installation
============

To use carthage add:
```
git "git@bitbucket.org:atlassian/swift-sqlite.git" "master"
```

Other swifty repositories
============
[swift-gcd-server](https://bitbucket.org/atlassian/swift-gcd-server)

[swift-http-server](https://bitbucket.org/atlassian/swift-http-server)

[swift-promises](https://bitbucket.org/atlassian/swift-promises)

Contributors
============

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
========

Copyright (c) 2016 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
