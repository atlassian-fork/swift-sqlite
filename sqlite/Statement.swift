//
//  Statement.swift
//  sqlite
//
//  Created by Nikolay Petrov on 10/27/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import Foundation

public struct SQLiteStatement {
    let connection: SQLiteConnection
    let sql: String

    var sqlite3_stmt: OpaquePointer?

    public init(connection: SQLiteConnection, sql: String) throws {
        self.connection = connection
        self.sql = sql

        try sqlite3_with(sqlite3_prepare_v2(connection.sqlite3_db, sql, -1, &sqlite3_stmt, nil))
    }

    public func blob(_ value: Data, to: Int) throws {
        try value.withUnsafeBytes { bytes in
            try sqlite3_with(sqlite3_bind_blob(sqlite3_stmt, Int32(to), bytes, Int32(value.count), nil))
        }
    }

    public func blobSize(_ value: Int32, to: Int) throws {
        try sqlite3_with(sqlite3_bind_zeroblob(sqlite3_stmt, Int32(to), value))
    }

    public func double(_ value: Double, to: Int) throws {
        try sqlite3_with(sqlite3_bind_double(sqlite3_stmt, Int32(to), value))
    }

    public func int(_ value: Int32, to: Int) throws {
        try sqlite3_with(sqlite3_bind_int(sqlite3_stmt, Int32(to), value))
    }

    public func int64(_ value: Int64, to: Int) throws {
        try sqlite3_with(sqlite3_bind_int64(sqlite3_stmt, Int32(to), value))
    }

    public func string(_ value: String, to: Int) throws {
        try sqlite3_with(sqlite3_bind_text(sqlite3_stmt, Int32(to), value, -1, nil))
    }

    public func null(to: Int) throws {
        try sqlite3_with(sqlite3_bind_null(sqlite3_stmt, Int32(to)))
    }

    public func execute(andClose shouldClose: Bool = true) throws {
        let execCode = sqlite3_step(sqlite3_stmt)
        guard execCode == SQLITE_DONE else {
            throw SQLiteError(code: execCode)
        }
        if shouldClose {
            try close()
        }
    }

    public func next() throws -> SQLiteResultRow? {
        return try SQLiteResultRow(statement: self)
    }

    public func reuse() throws {
        try reset()
        try clear()
    }

    public func reset() throws {
        try sqlite3_with(sqlite3_reset(sqlite3_stmt))
    }

    public func clear() throws {
        try sqlite3_with(sqlite3_clear_bindings(sqlite3_stmt))
    }

    public func close() throws {
        try sqlite3_with(sqlite3_finalize(sqlite3_stmt))
    }
}
