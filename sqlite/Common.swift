//
//  Common.swift
//  sqlite
//
//  Created by Nikolay Petrov on 10/27/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import Foundation

public struct SQLiteError: Error {
    let code: Int32

    var localizedDescription: String {
        guard let str = sqlite3_errstr(code) else {
            return "Unable to get error string for \(code)"
        }
        return String(cString: str)
    }
}
func sqlite3_with(_ callback: @autoclosure () -> Int32) throws {
    let resultCode = callback()
    guard resultCode == SQLITE_OK else {
        throw SQLiteError(code: resultCode)
    }
}

public enum SQLiteColumnType: Int32 {
    case integer = 1
    case float = 2
    case string = 3
    case blob = 4
    case null = 5
}

