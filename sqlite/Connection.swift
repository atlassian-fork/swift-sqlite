//
//  Connection.swift
//  sqlite
//
//  Created by Nikolay Petrov on 10/27/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import Foundation

public enum SQLiteConnectionTarget {
    case memory
    case temporary
    case filepath(String)

    fileprivate func sqlite3_uri() -> String {
        switch self {
        case .memory:
            return ":memory:"
        case .temporary:
            return ""
        case .filepath(let path):
            return path
        }
    }
}

public enum SQLiteConnectionMode {
    case read
    case readWrite
    case readWriteCreate

    fileprivate func sqlite3_flags() -> Int32 {
        switch self {
        case .read:
            return SQLITE_OPEN_READONLY
        case .readWrite:
            return SQLITE_OPEN_READWRITE
        case .readWriteCreate:
            return SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE
        }
    }
}

public enum SQLiteConnectionThreading {
    case multithreaded
    case serialized
    case `default`

    fileprivate func sqlite3_flags() -> Int32 {
        switch self {
        case .multithreaded:
            return SQLITE_OPEN_NOMUTEX
        case .serialized:
            return SQLITE_OPEN_FULLMUTEX
        case .`default`:
            return 0
        }
    }
}

public enum SQLiteConnectionCache {
    case shared
    case `private`
    case `default`

    fileprivate func sqlite3_flags() -> Int32 {
        switch self {
        case .shared:
            return SQLITE_OPEN_SHAREDCACHE
        case .private:
            return SQLITE_OPEN_PRIVATECACHE
        case .`default`:
            return 0
        }
    }
}

public struct SQLiteConnection {
    let target: SQLiteConnectionTarget

    var sqlite3_db: OpaquePointer?

    public init(target: SQLiteConnectionTarget,
         mode: SQLiteConnectionMode = .readWriteCreate,
         threading: SQLiteConnectionThreading = .serialized,
         cache: SQLiteConnectionCache = .`private`) throws {
        self.target = target

        let flags = mode.sqlite3_flags() | threading.sqlite3_flags() | cache.sqlite3_flags()
        try sqlite3_with(sqlite3_open_v2(target.sqlite3_uri(), &sqlite3_db, flags, nil))
    }

    public func statement(_ sql: String) throws -> SQLiteStatement {
        return try SQLiteStatement(connection: self, sql: sql)
    }

    public func close() throws {
        try sqlite3_with(sqlite3_close(sqlite3_db))
    }
}
